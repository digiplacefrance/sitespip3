<?PHP
		
	include "inclure/libmail.php";
	
	class Subscriber {
	   var $nom;
	   var $prenom;
	   var $civilite;
	   var $courriel;
	   var $key;
	   var $valide;
	   
			function Subscriber() {
				$this->nom       = "";
				$this->prenom    = "";
				$this->civilite  = "0";
				$this->courriel  = "";
				$this->key       = "";
				
				//E-mail validé par clic
				$this->valide    = 0;
			}
			
			function computeKey() {
				if ($this->key == "") {
					$this->key = crypt($this->courriel. microtime());
					$this->key = ereg_replace("[^A-Za-z0-9]", "", $this->key);
				}
			}

			function getCivilite() {
				$strtmp = "<font color='#CCCCCC'>Non renseignée</font>";
				switch($this->civilite) {
					case "1" : $strtmp = "Madame";       break;
					case "2" : $strtmp = "Mademoiselle"; break;
					case "3" : $strtmp = "Monsieur";     break;
				}
				return $strtmp;
			}

			function fillFromPost($post) {
				
				$this->nom      = (isset($post["nom"]))?trim($post["nom"]):"";
				$this->prenom   = (isset($post["prenom"]))?trim($post["prenom"]):"";
				$this->civilite = (isset($post["civilite"]))?trim($post["civilite"]):"0";
				$this->courriel = (isset($post["courriel"]))?strtolower(trim($post["courriel"])):"";
				/*
				$this->nom      = (isset($_POST["nom"]))?trim($_POST["nom"]):"";
				$this->prenom   = (isset($_POST["prenom"]))?trim($_POST["prenom"]):"";
				$this->civilite = (isset($_POST["civilite"]))?trim($_POST["civilite"]):"0";
				$this->courriel = (isset($_POST["courriel"]))?strtolower(trim($_POST["courriel"])):"";
				*/
			}

			function fillFromDatabase($row) {
				$this->nom      = $row->nom;
				$this->prenom   = $row->prenom;
				$this->civilite = $row->civilite;
				$this->courriel = $row->courriel;
				$this->key      = $row->key;
				$this->valide   = $row->valide;
			}
	
			function isInSession() {
				if (isset($_SESSION["courriel"])) {
					return true;
				}
				return false;
				
			}
	
			function fillFromSession() {
				$this->nom       = (isset($_SESSION["nom"]))?$_SESSION["nom"]:"";
				$this->prenom    = (isset($_SESSION["prenom"]))?$_SESSION["prenom"]:"";
				$this->civilite  = (isset($_SESSION["civilite"]))?$_SESSION["civilite"]:"";
				$this->courriel  = (isset($_SESSION["courriel"]))?$_SESSION["courriel"]:"";
				
			}
	
			function storeInSession() {
				$_SESSION["nom"]      = $this->nom;
				$_SESSION["prenom"]   = $this->prenom;
				$_SESSION["civilite"] = $this->civilite;
				$_SESSION["courriel"] = $this->courriel;
				
			}
	   
			function checkAfterPost() {
				if (strlen($this->nom) > 255) {
					echo "Nom invalide.";
					$ok = FALSE;
				}

				if (strlen($this->prenom) > 255) {
					echo "Prénom invalide.";
					$ok = FALSE;
				}
				
				if (strlen($this->courriel) == 0 || strlen($this->courriel) > 255) {
					echo "Courriel invalide.";
					$ok = FALSE;
				}
				
				if (!check_email_mx($this->courriel)) {
					echo "Courriel invalide.";
					$ok = FALSE;
				}
				
				if ($this->civilite != 0 && $this->civilite !=1 && $this->civilite!=2 && $this->civilite!=3) {
					echo "Civilité invalide.";
					$ok = FALSE;
				}
			}
	
	}

	//================================================================================================================

	function checkBanned($db, $ip) {
		$res = false;
		$sqlQuery = "SELECT * FROM rca_newsletter_client WHERE ip='$ip' AND banned='1'";
		$result = mysql_query($sqlQuery, $db) or die ("<p>erreur ".mysql_error ()."<br>".$sqlQuery."</p>");
		if ($row = mysql_fetch_object($result)) {
			$res = true;
			mysql_free_result($result);
		}
		
		return $res;
	}
	
	
	function check_email_mx($email) { 
		if( (preg_match('/(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/', $email)) || (preg_match('/^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?)$/',$email)) ) { 
			$host = explode('@', $email);
			if(checkdnsrr($host[1].'.', 'MX') ) return true;
			if(checkdnsrr($host[1].'.', 'A') ) return true;
			if(checkdnsrr($host[1].'.', 'CNAME') ) return true;
		}
		return false;
	}


	function insertOrUpdateClient($db, $ip) {
		$sqlQuery = "SELECT * FROM rca_newsletter_client WHERE ip='$ip'";
		$result = mysql_query($sqlQuery, $db) or die ("<p>erreur ".mysql_error ()."<br>".$sqlQuery."</p>");
		if ($row = mysql_fetch_object($result)) {
			mysql_free_result($result);
			$sqlQuery  = "UPDATE rca_newsletter_client SET `count`=`count`+1 WHERE `ip`='$ip'";
			mysql_query($sqlQuery, $db) or die ("<p>erreur ".mysql_error ()."<br>".$sqlQuery."</p>");
		} else {
			$sqlQuery  = "INSERT INTO rca_newsletter_client (`id_newsletter_client`, `ip`, `banned`, `count`) VALUES ('', '$ip', '0', '1')";
			mysql_query($sqlQuery, $db) or die ("<p>erreur ".mysql_error ()."<br>".$sqlQuery."</p>");
		}
	}
	

	function isSubscriberAlreadyExists($db, $subscriber) {
		$exisitingSubscriber = false;
		
		$sqlQuery = "SELECT * FROM rca_newsletter WHERE courriel='". $subscriber->courriel ."'";
		$result = mysql_query($sqlQuery, $db) or die ("<p>erreur ".mysql_error ()."<br>".$sqlQuery."</p>");
		
		if ($row = mysql_fetch_object($result)) {
			$exisitingSubscriber = new Subscriber();
			$exisitingSubscriber->fillFromDatabase($row);
			mysql_free_result($result);
		}
		
		return $exisitingSubscriber;
	}

	function insertSubscriber($db, $subscriber, $ip) {
		$sqlQuery  = "INSERT INTO rca_newsletter (`id_newsletter`, `courriel`, `nom`, `prenom`, `civilite`, `valide`, `key`, `ip`) VALUES ('', "
			." '". str_replace("'", "''", $subscriber->courriel) ."' ,"
			." '". str_replace("'", "''", $subscriber->nom)      ."' ,"
			." '". str_replace("'", "''", $subscriber->prenom)   ."' ,"
			." '". str_replace("'", "''", $subscriber->civilite) ."' ,"
			." 0                                         ,"
			." '". str_replace("'", "''", $subscriber->key) ."'      ,"
			." '". str_replace("'", "''", $ip) ."')";

		mysql_query($sqlQuery, $db) or die ("<p>erreur ".mysql_error ()."<br>".$sqlQuery."</p>");
	}



	function sendMailToNewSubscriber($subscriber) {
		$from    = "communication@canalacademie.com";
		$to      = $subscriber->courriel;
		$subject = "Canal Académie - Abonnement à notre lettre d'information";
		
		$message = "Bonjour";
		if ($subscriber->civilite != "0") {
			$message .= " ".$subscriber->getCivilite();
		}

		if ($subscriber->nom != "") {
			$message .= " ".$subscriber->nom;
		}
		
		$message .= ",\n\n"
			."Merci de cliquer sur le lien suivant pour confirmer votre inscription :\n"
			."http://www.canalacademie.com/squelettes/abonnement_message_4.php?k=". urlencode($subscriber->key)
			." \n\n"
			."Si vous recevez ce message par erreur merci de nous le signaler en cliquant sur le lien suivante :\n"
			."http://www.canalacademie.com/squelettes/abonnement_message_4.php?abuse=1&k=". urlencode($subscriber->key)
			."\n\n\n\n"
			."A bientôt dans votre boîte aux lettres,\n\n"
			."L'équipe de Canal Académie\n"
			."Canal Académie est la première radio académique francophone sur Internet\n"
			."http://www.canalacademie.com/ \n\n"
			."\n\n\n"
			."Les informations recueillies vous concernant font l’objet d’un traitement informatique destiné à l'envoi de la lettre d'information électronique Message.\n Conformément à la loi «informatique et libertés» du 6 janvier 1978, vous bénéficiez d’un droit d’accès, de rectification et de suppression aux informations qui vous concernent. Si vous souhaitez exercer ce droit, veuillez vous adresser au service communication de Canal Académie : communication@canalacademie.com.\n\n";
		
		$m = new Mail;
		$m->From($from);
		$m->To($to);
		$m->Subject($subject);
		$m->Body($message);
		$m->Send();
	}


	function sendMailToOldSubscriber($subscriber) {
		$from    = "communication@canalacademie.com";
		$to      = $subscriber->courriel;
		$subject = "Canal Académie - Lettre d'information";
		
		$message = "Bonjour";
		if ($subscriber->civilite != "0") {
			$message .= " ".$subscriber->getCivilite();
		}

		if ($subscriber->nom != "") {
			$message .= " ".$subscriber->nom;
		}
		
		if ($subscriber->valide == 1) {
			$message .= ",\n\n"
				."Votre inscription à notre lettre d'information est déjà confirmée.\n";
		} else {
			$message .= ",\n\n"
				."Merci de cliquer sur le lien suivant pour confirmer votre inscription :\n"
				."http://www.canalacademie.com/squelettes/abonnement_message_4.php?k=". urlencode($subscriber->key)
				." \n\n"
				."Si vous recevez ce message par erreur merci de nous le signaler en cliquant sur le lien suivante :\n"
				."http://www.canalacademie.com/squelettes/abonnement_message_4.php?abuse=1&k=". urlencode($subscriber->key);
		}
		
		$message .= "\n\n\n\n"
			."A bientôt dans votre boîte aux lettres,\n\n"
			."L'équipe de Canal Académie\n"
			."Canal Académie est la première radio académique francophone sur Internet\n"
			."http://www.canalacademie.com/ \n\n"
			."\n\n\n\n"
			."Les informations recueillies vous concernant font l’objet d’un traitement informatique destiné à l'envoi de la lettre d'information électronique Message.\n Conformément à la loi «informatique et libertés» du 6 janvier 1978, vous bénéficiez d’un droit d’accès, de rectification et de suppression aux informations qui vous concernent. Si vous souhaitez exercer ce droit, veuillez vous adresser au service communication de Canal Académie : communication@canalacademie.com.\n\n";
		
		$m = new Mail;
		$m->From($from);
		$m->To($to);
		$m->Subject($subject);
		$m->Body($message);
		$m->Send();
	}
?>