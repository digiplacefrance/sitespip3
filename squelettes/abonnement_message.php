                   
<?php
	session_start();
	include_once ("abonnement_message_lib.php");
	
	$ip = $_SERVER["REMOTE_ADDR"];
	include_once ("./inclure/db_inc.php");
	$db = mysql_connect ($db_server, $db_user, $db_pwd) or die ("erreur de connection");
	
	if (!mysql_select_db ($db_name, $db)) {
		print ("erreur ".mysql_error ()."<br>");
		mysql_close ($db);
		exit;
	}

	if (checkBanned($db, $_SERVER["REMOTE_ADDR"])) {
		echo "Vous n'êtes plus autorisé à accéder à cette page.<br>";
		echo "Contactez-nous pour débloquer votre accès.<br><br>";
		echo "L'équipe de Canal Académie<br>";
		mysql_close ($db);
		@exit();
	}
	
	$subscriber = new Subscriber();
	
	if (empty($_GET["new"])) {
		$subscriber->fillFromSession();
	}
	
	$ok = FALSE;
		
	if (isset($_POST["formAction"])) {
		$subscriber->fillFromPost($_POST);
		$ok = TRUE;
		$subscriber->storeInSession();
		
		
		if (strlen($subscriber->courriel) == 0 || strlen($subscriber->courriel) > 255) {
			$err = "Courriel invalide.";
			$subscriber->fillFromSession();
			$ok = FALSE;
		}
				
		if (!check_email_mx($subscriber->courriel)) {
			$err = "Courriel invalide.";
			$subscriber->fillFromSession();
			$ok = FALSE;
		}
	}
	
	
	
	if ($ok) {
		@header("Location: abonnement_message_2.php");
		@exit();
	}
?>
<html>
<head>
<title>Canal Acad&eacute;mie | S'abonner &agrave; Message (&eacute;tape 1/2)</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="Pragma"  CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="0">
<style type="text/css">
<!--
td {  font-size: 12px; font-family: Georgia, "Times New Roman", Times, serif}
-->
</style>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="2" marginwidth="2">
<div align="center"> 
  <table border="0" cellspacing="0" bgcolor="#ffffff" width="800" style='border:1px solid #000;'>
  	<tr>
  		<td>
  			<img src="./message/images/new_bandeau.jpg" width="800" height="140" border="0" />
  		</td>
  	</tr>
    <tr> 
      <td bgcolor="#ffffff" width="800" align="center"><br/> <span style="font-weight:bold; font-size:18px; color:#000; font-family: Georgia">RECEVEZ 
        GRATUITEMENT NOTRE LETTRE D'INFORMATION</span> </td>
    </tr>
    <tr> 
      <td width="800">
      <br/> 
        <p style="margin-left:10px"> 
           Tous les mardi, recevez notre lettre d'information électronique. Inscrivez-vous d&egrave;s 
          &agrave; pr&eacute;sent en nous indiquant vos coordonn&eacute;es ci-dessous.</p>
        <p style="margin-left:10px">A bient&ocirc;t dans votre bo&icirc;te aux lettres.<br>
          L'&eacute;quipe de Canal Acad&eacute;mie.<br>
        </p>
      </td>
    </tr>
    <tr> 
      <td align="center" width="800"><font face="Trebuchet MS"><br>
        </font> 
        <form name="formMessage" method="post" action="abonnement_message_1.php" style="margin:0px; padding:0px">
          <table width="786" border="0">
            <tr> 
              <td><b>Votre courriel <font color="#FF0000">*</font></b></td>
              <td> 
              	
                <input type="text" name="courriel" maxlength="255" size="50" value="<? echo htmlentities($subscriber->courriel); ?>">
                
              </td>
            </tr>
            <!--
            <tr> 
              <td><b>Votre civilit&eacute;</b></td>
              <td> 
                <select name="civilite">
                  <option value="0" <? echo ($subscriber->civilite == "0")?"selected":""; ?>>Votre 
                  civilit&eacute;</option>
                  <option value="1" <? echo ($subscriber->civilite == "1")?"selected":""; ?>>Madame</option>
                  <option value="2" <? echo ($subscriber->civilite == "2")?"selected":""; ?>>Mademoiselle</option>
                  <option value="3" <? echo ($subscriber->civilite == "3")?"selected":""; ?>>Monsieur</option>
                </select>
              </td>
            </tr>
            <tr> 
              <td><b>Votre nom</b></td>
              <td> 
                <input type="text" name="nom" maxlength="255" size="50"  value="<? echo htmlentities($subscriber->nom); ?>">
              </td>
            </tr>
            <tr> 
              <td><b>Votre pr&eacute;nom</b></td>
              <td> 
                <input type="text" name="prenom" maxlength="255" size="50"  value="<? echo htmlentities($subscriber->prenom); ?>">
              </td>
            </tr>
            -->
            <tr align="center"> 
              <td colspan="2"> 
                <input type="hidden" name="formAction" value="post">
                <input type="submit" name="bSubmit" value="Valider &gt;&gt;">
              </td>
            </tr>
          </table>
        </form>
        <br>
      </td>
    </tr>
    <tr>
    	<td align="left">
    		<p style='margin:10px;'>
	    		
	    			<strong><a href='http://www.canal-academie.net/newsletter/index2.php' style='color:#e35049'>Cliquer ici pour abonner vos amis</a></strong>
	    		
    		</p>
    	</td>
    </tr>
    <tr> 
      <td width="800" align="center"> <font face="Arial, Helvetica, sans-serif" style="font-size:11">Vous 
        disposez d'un droit d'acc&egrave;s, de modification, de rectification 
        et de suppression des donn&eacute;es qui vous concernent (art. 34 de la 
        loi Informatique et libert&eacute;s).<br/> Pour l'exercer, adressez-vous &agrave; 
        <br>
        <b>Canal Acad&eacute;mie <br>
        23, quai Conti <br>
        75006 Paris </b></font> <br>
        <div align="center">
        	<br/>
          <input type="button" name="Button" value="Fermer" onClick="if (opener != null) opener.focus(); window.close();">
        </div>
      </td>
    </tr>
  </table>
</div>
<script language="JavaScript" type="text/javascript">
	document.formMessage.courriel.focus();
</script>
</body>
</html>


