<?php


$chemin = 'IMG/';

if(empty($_GET['format'])){
	$def_largeur = 83;
	$def_hauteur = 83;
}
else{
	$def_largeur = 285;
	$def_hauteur = 285;
	$chemin .= $_GET['format'].'/';
}

// Ajout d'une condition car pour le guide on va chercher des images en absolu...
if(strstr($_GET['img'], 'http://') === false)
	$image = $chemin.$_GET['img'];
else
	$image = $_GET['img'];

// Format de l'image
$info_img = getimagesize($image);
header('Content-type: '.$info_img['mime']);

// On cr�e une image en m�moire contenant l'image originale
$source = false;
switch($info_img['mime']) {
	
	case 'image/jpeg' : $source = imagecreatefromjpeg($image); break;
	case 'image/gif'  : $source = imagecreatefromgif($image);  break;
	case 'image/png'  : $source = imagecreatefrompng($image);  break;
	default: $source = false;
}

if($source !== false) {
	
	// L'image est-elle sup�rieures aux dimensions requises ?
	if($info_img[0] > $def_largeur or $info_img[1] > $def_hauteur) {
		
		// Calcul des dimensions de la vignette
		if($info_img[0] > $info_img[1]) {
			
			$largeur = $def_largeur;
			$hauteur = ($info_img[1] * $def_largeur) / $info_img[0];
		}
		else {
			
			$hauteur = $def_hauteur;
			$largeur = ($info_img[0] * $def_hauteur) / $info_img[1];
		}
		
		if($hauteur > $def_hauteur) {
			
			$hauteur = $def_hauteur;
			$largeur = ($info_img[0] * $def_hauteur) / $info_img[1];
		}
	}
	else {
		$largeur = $info_img[0];
		$hauteur = $info_img[1];
	}


	// Cr�ation de la vignette
	$vignette = imagecreatetruecolor($largeur, $hauteur);
	imagecopyresampled($vignette, $source, 0, 0, 0, 0, $largeur, $hauteur, $info_img[0], $info_img[1]);


	// Affichage
	switch($info_img['mime']) {
		
		case 'image/jpeg' : imagejpeg($vignette, '', 90); break;
		case 'image/gif'  : imagegif($vignette, '');      break;
		case 'image/png'  : imagepng($vignette, '', 9);   break;
	}

	// Destruction des ressources
	imagedestroy($source);
	imagedestroy($vignette);
}
?> 